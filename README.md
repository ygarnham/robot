# Toy Robot

## Note
Please note that Ruby is not my native language, I come from a .NET/C# background, so some of the structure or decisions
may seem odd.  My apologies.  I realise I'm probably in cases making things more difficult than they need to be because
I don't know the shortcut/existing convention in Ruby

Ruby version 2.0.0p576 used

## Design decisions

* Testable
* Dependencies for the table and the output stream are passed to the "service implementation", but
robot implementation hidden from the consumer
* Tended towards a service model rather than a purely object oriented model, so the service and the validator are
doing all of the work, the table and robot models are fairly concise
* Commands are not case sensitive
* F parameter in the PLACE command is not case sensitive
* Allow abbreviations of the F parameter - i.e. N,S,E,W as well as NORTH,SOUTH,EAST,WEST

[Change log](https://bitbucket.org/ygarnham/robot/commits/branch/master)

Installation
```shell
$ bundle install
```

Test
```shell
$ rspec
```

Verbose test
```shell
$ rspec -f d
```

Run
```shell
$ ruby application.rb
```

## Original Problem Description

- The application is a simulation of a toy robot moving on a square tabletop,
  of dimensions 5 units x 5 units.
- There are no other obstructions on the table surface.
- The robot is free to roam around the surface of the table, but must be
  prevented from falling to destruction. Any movement that would result in the
  robot falling from the table must be prevented, however further valid
  movement commands must still be allowed.

Create an application that can read in commands of the following form:

    PLACE X,Y,F
    MOVE
    LEFT
    RIGHT
    REPORT

- PLACE will put the toy robot on the table in position X,Y and facing NORTH,
  SOUTH, EAST or WEST.
- The origin (0,0) can be considered to be the SOUTH WEST most corner.
- The first valid command to the robot is a PLACE command, after that, any
  sequence of commands may be issued, in any order, including another PLACE
  command. The application should discard all commands in the sequence until
  a valid PLACE command has been executed.
- MOVE will move the toy robot one unit forward in the direction it is
  currently facing.
- LEFT and RIGHT will rotate the robot 90 degrees in the specified direction
  without changing the position of the robot.
- REPORT will announce the X,Y and F of the robot. This can be in any form,
  but standard output is sufficient.

- A robot that is not on the table can choose the ignore the MOVE, LEFT, RIGHT
  and REPORT commands.
- Input can be from a file, or from standard input, as the developer chooses.
- Provide test data to exercise the application.

Constraints
-----------

- The toy robot must not fall off the table during movement. This also
  includes the initial placement of the toy robot.
- Any move that would cause the robot to fall must be ignored.

Example Input and Output
------------------------

### Example a

    PLACE 0,0,NORTH
    MOVE
    REPORT

Expected output:

    0,1,NORTH

### Example b

    PLACE 0,0,NORTH
    LEFT
    REPORT

Expected output:

    0,0,WEST

### Example c

    PLACE 1,2,EAST
    MOVE
    MOVE
    LEFT
    MOVE
    REPORT

Expected output

    3,3,NORTH

Deliverables
------------

The Ruby source files, the test data and any test code.

It is not required to provide any graphical output showing the movement of
the toy robot.