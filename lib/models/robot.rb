module Robot
  module Models
    class Robot
      attr_accessor :x_coord, :y_coord, :facing

      def get_direction_from_character dir
        if dir && dir.size == 1 then
          case dir.downcase
            when 'n' then :north
            when 'e' then :east
            when 'w' then :west
            when 's' then :south
            else nil
          end
        end
      end

      def directions
        @directions = [:north, :east, :south, :west]
      end
    end
  end
end
