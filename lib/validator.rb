module Robot
  class Validator
    def is_robot_command(command, args = nil)
      is_valid = false
      if command && !command.to_s.empty? then
        cmd = command.to_s.downcase
        if cmd == 'place'
            #check if args is array and size=3
            is_valid = are_place_args_valid(args)
        elsif cmd == 'report' || cmd == 'move' || cmd == 'left' || cmd == 'right'
          if args.nil? then
            is_valid = true
          end
        end
      end
      is_valid
    end

    def is_position_valid(table_width, table_height, x_coord, y_coord)
      position_valid = false
      if positive_integer(table_width) && positive_integer(table_height) && positive_integer(x_coord) && positive_integer(y_coord) then
        #note that coords are 0 based, so coords need to be less than width/height
        if Integer(x_coord) < Integer(table_width) && Integer(y_coord) < Integer(table_height) then
          position_valid = true
        end
      end
      position_valid
    end

    def is_move_valid(table_width, table_height, robot)
      move_valid = false
      if robot.facing then
        case robot.facing
          when :north
            move_valid = is_position_valid(table_width, table_height, robot.x_coord, robot.y_coord + 1)
          when :south
            move_valid = is_position_valid(table_width, table_height, robot.x_coord, robot.y_coord - 1)
          when :east
            move_valid = is_position_valid(table_width, table_height, robot.x_coord + 1, robot.y_coord)
          when :west
            move_valid = is_position_valid(table_width, table_height, robot.x_coord - 1, robot.y_coord)
        end
      end
      move_valid
    end


    def positive_integer(number)
      is_positive_integer = false
      int = Integer(number) rescue -1
      if(int >= 0)
        is_positive_integer = true
      end
      is_positive_integer
    end

    private
    def are_place_args_valid(args)
      is_valid = false
      if args && args.kind_of?(Array) && args.size == 3
        if positive_integer(args[0]) && positive_integer(args[1]) && is_direction(args[2])
          is_valid = true
        end
      end
      is_valid
    end


    def is_direction(facing)
      is_direction = false
      if facing && !facing.to_s.empty?
        dir = facing.to_s.downcase
        if dir == 'north' || dir == 'south' || dir == 'west' || dir == 'east'
            is_direction = true
        elsif dir == 'n' || dir == 's' || dir == 'w' || dir == 'e'
            is_direction = true
        end
      end
      is_direction
    end
  end
end
