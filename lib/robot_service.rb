module Robot
  class RobotService
    attr_accessor :robot, :table, :validator

    def initialize(table, output_stream = $stdout)
      @table = table
      @output_stream = output_stream
      @robot = Robot::Models::Robot.new
      @validator = Robot::Validator.new
    end

    def parse_command input
      command = nil
      args = nil

      if input && !input.to_s.empty? then
        input_split = input.to_s.strip.split
        if input_split && input_split.size > 0 then
          command = input_split.shift
          #if there are still items in the array, we have args
          if input_split.size > 0 then
            args_joined = ''
            #firstly join the string back together after it was space separated
            input_split.each { |arg| args_joined.concat(arg.to_s.concat(' ')) }
            #now split it by comma
            args = args_joined.to_s.split(',').map(&:strip)
          end
        end
      end

      return command, args
    end

    def execute_command input
      command, args = parse_command input
      if can_action_command(command, args) then
        run_command command, args
      end
    end

    def can_action_command command, args
          is_environment_valid &&
          @validator.is_robot_command(command, args) &&
          can_robot_make_move(command, args )
    end

    def is_environment_valid
      environment_valid = false
      if @table && @robot then
        if @validator.positive_integer(@table.width) && @validator.positive_integer(@table.height) then
          if @table.width > 0 && @table.height > 0 then
            environment_valid = true
          end
        end
      end
      environment_valid
    end

    def can_robot_make_move command, args = nil
      command_valid = false
      command_sym = command.downcase.to_sym
      case command_sym
        when :place then
          command_valid = @validator.is_position_valid(@table.width, @table.height, args[0], args[1])
        else
          if @robot.x_coord then
            #if the robot is on the table, the only command we need to validate is move
            if command_sym == :move then
              command_valid = @validator.is_move_valid(@table.width, @table.height, @robot)
            else
              command_valid = true
            end
          end
      end
      command_valid
    end

    def place_robot args
      if args.size == 3 then
        @robot.x_coord = Integer(args[0])
        @robot.y_coord = Integer(args[1])
        index = @robot.directions.index(args[2].downcase.to_sym)
        direction = nil
        if index then
          direction = @robot.directions[index]
        else
          direction = @robot.get_direction_from_character(args[2])
        end

        if direction
          @robot.facing = direction
        end
      end
    end

    def robot_left
      if @robot.x_coord && @robot.y_coord && @robot.facing then
        index = @robot.directions.index(@robot.facing)
        if index then
          @robot.facing = @robot.directions.rotate(index - 1)[0]
        end
      end
    end

    def robot_right
      if @robot.x_coord && @robot.y_coord && @robot.facing then
        index = @robot.directions.index(@robot.facing)
        if index then
          @robot.facing = @robot.directions.rotate(index + 1)[0]
        end
      end
    end

    def report_position
      position = 'ROBOT NOT PLACED'
      if @robot.x_coord && @robot.y_coord && @robot.facing then
        position = "#{@robot.x_coord},#{@robot.y_coord},#{@robot.facing.to_s.upcase}"
      end
      position
    end

    def move_robot
      case @robot.facing
        when :north
          @robot.y_coord = @robot.y_coord + 1
        when :south
          @robot.y_coord = @robot.y_coord - 1
        when :west
          @robot.x_coord = @robot.x_coord - 1
        when :east
          @robot.x_coord = @robot.x_coord + 1
      end
    end

    def run_command command, args
      case command.downcase.to_sym
        when :place then
          place_robot(args)
        when :report then
          @output_stream.puts report_position
        when :left then
          robot_left
        when :right then
          robot_right
        when :move then
          move_robot
      end
    end
  end
end
