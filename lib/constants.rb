module Robot
  module Constants
    ROBOT_DEFINITION = "Yael's robot app\n"\
    "________________\n"\
    "Commands:\n"\
    "PLACE X,Y,F\n"\
    "MOVE\n"\
    "LEFT\n"\
    "RIGHT\n"\
    "REPORT\n"\
    "ctr-c or EXIT to exit\n"\
    "_____________________\n\n"
  end
end
