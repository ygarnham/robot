require_relative 'lib/robot'
definition =  Robot::Constants.const_get(:ROBOT_DEFINITION)

puts definition
$stdout.flush

table = Robot::Models::Table.new
service = Robot::RobotService.new(table)
input = gets.chomp
while input
  if input.downcase == 'exit' then
    break
  end
  service.execute_command(input)
  input = gets.chomp
end
