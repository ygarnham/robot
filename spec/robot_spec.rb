require_relative 'spec_helper'

module Robot
  module Models
    describe Robot do

      before :each do
        @robot = Robot.new
      end

      context 'translate requested direction' do
        it 'direction empty returns nil' do
          expect(@robot.get_direction_from_character('')).to be_nil
        end

        it 'direction nil returns nil' do
          expect(@robot.get_direction_from_character(nil)).to be_nil
        end

        it 'direction number returns nil' do
          expect(@robot.get_direction_from_character(2)).to be_nil
        end

        it 'direction string length > 1 returns nil' do
          expect(@robot.get_direction_from_character('north')).to be_nil
        end

        it 'direct b returns nil' do
          expect(@robot.get_direction_from_character('b')).to be_nil
        end

        it 'direction N translates to :north' do
          expect(@robot.get_direction_from_character('N')).to eq :north
        end

        it 'direction n translates to :north' do
          expect(@robot.get_direction_from_character('n')).to eq :north
        end

      end
    end
  end
end
