require_relative 'spec_helper'

module Robot
  describe Validator do

    before :each do
      @validator = Validator.new
    end

    context 'Is Robot Command' do
      it 'should return false if command is empty string' do
        expect(@validator.is_robot_command('')).to eq false
      end

      it 'should return false if command is nil' do
        expect(@validator.is_robot_command(nil)).to eq false
      end

      it 'should return false if command is numeric' do
        expect(@validator.is_robot_command(2)).to eq false
      end

      context 'Is Place Command Correct' do
        it 'should return false if command is place but args are empty array' do
          expect(@validator.is_robot_command('place', '')).to eq false
        end

        it 'should return false if command is place but args is nil' do
          expect(@validator.is_robot_command('place')).to eq false
        end

        it 'should return false if command is place but args is not array' do
          expect(@validator.is_robot_command('place', '1,2,NORTH')).to eq false
        end

        it 'should return false if command is place but args length not 3' do
          expect(@validator.is_robot_command('place',[1,2])).to eq false
        end

        it 'should return false if command is place but first arg is not numeric' do
          expect(@validator.is_robot_command('place',['a',2,'NORTH'])).to eq false
        end

        it 'should return false if command is place but first arg is negative' do
          expect(@validator.is_robot_command('place',[-1,2,'NORTH'])).to eq false
        end

        it 'should return false if command is place but second arg is not numeric' do
          expect(@validator.is_robot_command('place',[1,'a','NORTH'])).to eq false
        end

        it 'should return false if command is place but second arg is negative' do
          expect(@validator.is_robot_command('place',[1,-2,'NORTH'])).to eq false
        end

        it 'should return false if command is place but third arg is numeric' do
          expect(@validator.is_robot_command('place',[1,2,3])).to eq false
        end

        it 'should return false if command is place but third arg is string but not direction' do
          expect(@validator.is_robot_command('place',[1,2,'NORTHY'])).to eq false
        end

        it 'should return true if command is place, first two args are numeric and third is direction' do
          expect(@validator.is_robot_command('place',[1,2,'NORTH'])).to eq true
        end

        it 'should return true if command is PLACE, first two args are numeric and third is direction' do
          expect(@validator.is_robot_command('PLACE',[1,2,'NORTH'])).to eq true
        end

        it 'should return true if command is mixed case place, first two args are numeric and third is direction' do
          expect(@validator.is_robot_command('PlaCE',[1,2,'NORTH'])).to eq true
        end
      end

      context 'Is Other Command Valid' do
        it 'should return false if command is string but isnt known' do
          expect(@validator.is_robot_command('test')).to eq false
        end

        it 'should return false if command is report but args not nil' do
          expect(@validator.is_robot_command('report', '')).to eq false
        end

        it 'should return false if command is report but args array' do
          expect(@validator.is_robot_command('report', [])).to eq false
        end

        it 'should return false if command is move but args not nil' do
          expect(@validator.is_robot_command('move', '')).to eq false
        end

        it 'should return false if command is move but args array' do
          expect(@validator.is_robot_command('move', [])).to eq false
        end

        it 'should return false if command is left but args not nil' do
          expect(@validator.is_robot_command('left', '')).to eq false
        end

        it 'should return false if command is left but args array' do
          expect(@validator.is_robot_command('left', [])).to eq false
        end

        it 'should return false if command is right but args not nil' do
          expect(@validator.is_robot_command('right', '')).to eq false
        end

        it 'should return false if command is right but args array' do
          expect(@validator.is_robot_command('right', [])).to eq false
        end

        it 'should return true if command is report and args is nil' do
          expect(@validator.is_robot_command('report')).to eq true
        end

        it 'should return true if command is move and args is nil' do
          expect(@validator.is_robot_command('move')).to eq true
        end

        it 'should return true if command is left and args is nil' do
          expect(@validator.is_robot_command('left')).to eq true
        end

        it 'should return true if command is right and args is nil' do
          expect(@validator.is_robot_command('right')).to eq true
        end
      end
    end

    context 'Is Positive Integer' do
      it 'should return false when input is nil' do
        expect(@validator.positive_integer(nil)).to eq false
      end

      it 'should return false when input is empty string' do
        expect(@validator.positive_integer('')).to eq false
      end

      it 'should return false when input is negative' do
        expect(@validator.positive_integer(-1)).to eq false
      end

      it 'should return false when input is string' do
        expect(@validator.positive_integer('a')).to eq false
      end

      it 'should return true when input is 0' do
        expect(@validator.positive_integer(0)).to eq true
      end

      it 'should return true when input is positive' do
        expect(@validator.positive_integer(1)).to eq true
      end

    end

    context 'Is Place Command Valid' do
      it 'should return false when place command x position puts robot outside bounds' do
        table_width = 3
        table_height = 2
        x = 3 #0 based
        y = 1
        expect(@validator.is_position_valid(table_width, table_height, x, y)).to eq false
      end

      it 'should return false when place command y position puts robot outside bounds' do
        table_width = 3
        table_height = 2
        x = 2 #0 based
        y = 2
        expect(@validator.is_position_valid(table_width, table_height, x, y)).to eq false
      end

      it 'should return true when place command x & y positions puts robot within table bounds' do
        table_width = 3
        table_height = 2
        x = 2 #0 based
        y = 1
        expect(@validator.is_position_valid(table_width, table_height, x, y)).to eq true
      end
    end

    context 'Is Move Command Valid' do
      it 'should return false when robot facing east and move puts robot outside bounds' do
        robot = Models::Robot.new
        robot.x_coord = 4
        robot.y_coord = 0
        robot.facing = :east
        expect(@validator.is_move_valid(5,5,robot)).to eq false
      end

      it 'should return false when robot facing west and move puts robot outside bounds' do
        robot = Models::Robot.new
        robot.x_coord = 0
        robot.y_coord = 0
        robot.facing = :west
        expect(@validator.is_move_valid(5,5,robot)).to eq false
      end

      it 'should return false when robot facing north and move puts robot outside bounds' do
        robot = Models::Robot.new
        robot.x_coord = 0
        robot.y_coord = 4
        robot.facing = :north
        expect(@validator.is_move_valid(5,5,robot)).to eq false
      end

      it 'should return false when robot facing south and move puts robot outside bounds' do
        robot = Models::Robot.new
        robot.x_coord = 0
        robot.y_coord = 0
        robot.facing = :south
        expect(@validator.is_move_valid(5,5,robot)).to eq false
      end

      it 'should return true when robot facing east and move puts robot within table bounds' do
        robot = Models::Robot.new
        robot.x_coord = 3
        robot.y_coord = 0
        robot.facing = :east
        expect(@validator.is_move_valid(5,5,robot)).to eq true
      end

      it 'should return true when robot facing west and move puts robot within table bounds' do
        robot = Models::Robot.new
        robot.x_coord = 1
        robot.y_coord = 0
        robot.facing = :west
        expect(@validator.is_move_valid(5,5,robot)).to eq true
      end

      it 'should return true when robot facing north and move puts robot within table bounds' do
        robot = Models::Robot.new
        robot.x_coord = 0
        robot.y_coord = 3
        robot.facing = :north
        expect(@validator.is_move_valid(5,5,robot)).to eq true
      end

      it 'should return true when robot facing south and move puts robot within table bounds' do
        robot = Models::Robot.new
        robot.x_coord = 0
        robot.y_coord = 1
        robot.facing = :south
        expect(@validator.is_move_valid(5,5,robot)).to eq true
      end
    end
  end
end
