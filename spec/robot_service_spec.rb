require_relative 'spec_helper'

module Robot
  describe RobotService do

    before :each do
      table = Robot::Models::Table.new
      @service = RobotService.new(table)
    end

    context 'Parse Command' do
      it 'should return nil when command entered is nil' do
        command, args = @service.parse_command(nil)
        expect(command).to be_nil
      end

      it 'should return nil when command entered is empty' do
        command, args = @service.parse_command('')
        expect(command).to be_nil
      end

      it 'should return command string but args nil when command entered is number' do
        command, args = @service.parse_command(2)
        expect(command).to eq '2'
      end

      it 'should return command string and args length one when two words seperated by space entered' do
        command, args = @service.parse_command('robot test')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 1
        expect(args[0]).to eq 'test'
      end

      it 'should return command string and args length one when three words seperated by spaces entered' do
        command, args = @service.parse_command('robot test left')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 1
        expect(args[0]).to eq 'test left'
      end

      it 'should return command string and args length one when two words seperated by two spaces entered' do
        command, args = @service.parse_command('robot  test')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 1
        expect(args[0]).to eq 'test'
      end

      it 'should return command string and args length one when two words seperated by three spaces entered' do
        command, args = @service.parse_command('robot   test')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 1
        expect(args[0]).to eq 'test'
      end

      it 'should return command string and args length two when two words seperated by space then comma entered' do
        command, args = @service.parse_command('robot  test,left')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 2
        expect(args[0]).to eq 'test'
        expect(args[1]).to eq 'left'
      end

      it 'should return command string and args length two when two words seperated by space then space and comma entered' do
        command, args = @service.parse_command('robot  test, left')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 2
        expect(args[0]).to eq 'test'
        expect(args[1]).to eq 'left'
      end

      it 'should trim spaces from args' do
        command, args = @service.parse_command('robot  test, left , right')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 3
        expect(args[0]).to eq 'test'
        expect(args[1]).to eq 'left'
        expect(args[2]).to eq 'right'
      end

      it 'should return entry in args array when multiple commas entered' do
        command, args = @service.parse_command('robot  , , right')
        expect(command).to eq 'robot'
        expect(args.kind_of?(Array)).to eq true
        expect(args.size).to eq 3
        expect(args[0].empty?).to eq true
        expect(args[1].empty?).to eq true
        expect(args[2]).to eq 'right'
      end

    end

    context 'Is Valid Command' do
      it 'should return false if table is nil' do
        @service.table = nil
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      it 'should return false if robot is nil' do
        @service.robot = nil
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      it 'should return false if positive integer check returns false' do
        allow(@service.validator).to receive(:positive_integer).and_return(false)
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      it 'should return false if table width is 0' do
        @service.table.width = 0
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      it 'should return false if table height is 0' do
        @service.table.width = 0
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      it 'should return false when Is Robot Command returns false' do
       allow(@service.validator).to receive(:is_robot_command).and_return(false)
        expect(@service.can_action_command('place', [1,2,'N'])).to eq false
      end

      context 'Robot not on table' do
        it 'should return false when command is place and Is Place Valid returns false' do
          allow(@service.validator).to receive(:is_position_valid).and_return(false)
          expect(@service.can_robot_make_move('place', [1,2,'N'])).to eq false
        end

        it 'should return true when command is place and Is Place Valid returns true' do
          allow(@service.validator).to receive(:is_position_valid).and_return(true)
          expect(@service.can_robot_make_move('place', [1,2,'N'])).to eq true
        end

        it 'should return false when Robot Command is correct syntax command is move' do
          expect(@service.can_robot_make_move('move')).to eq false
        end

        it 'should return false when Robot Command is correct syntax command is left' do
          expect(@service.can_robot_make_move('left')).to eq false
        end

        it 'should return false when Robot Command is correct syntax command is right' do
          expect(@service.can_robot_make_move('right')).to eq false
        end

        it 'should return false when Robot Command is correct syntax command is report' do
          expect(@service.can_robot_make_move('report')).to eq false
        end
      end

      context 'Robot on table' do
        before :each do
          @service.robot.facing = 'north'
          @service.robot.x_coord = 2
          @service.robot.y_coord = 2
        end

        it 'should return false when command is place and Is Place Valid returns false' do
          allow(@service.validator).to receive(:is_position_valid).and_return(false)
          expect(@service.can_robot_make_move('place', [1,2,'N'])).to eq false
        end

        it 'should return true when command is place and Is Place Valid returns true' do
          allow(@service.validator).to receive(:is_position_valid).and_return(true)
          expect(@service.can_robot_make_move('place', [1,2,'N'])).to eq true
        end

        it 'should return true when command is report' do
          expect(@service.can_robot_make_move('report')).to eq true
        end

        it 'should return true when command is left' do
          expect(@service.can_robot_make_move('left')).to eq true
        end

        it 'should return true when command is right' do
          expect(@service.can_robot_make_move('right')).to eq true
        end

        it 'should return false when command is move and Is Move Valid returns false' do
          allow(@service.validator).to receive(:is_move_valid).and_return(false)
          expect(@service.can_robot_make_move('move')).to eq false
        end

        it 'should return true when command is move and Is Move Valid returns true' do
          allow(@service.validator).to receive(:is_move_valid).and_return(true)
          expect(@service.can_robot_make_move('move')).to eq true
        end
      end
    end

    context 'Report Location' do
      it 'robot not placed' do
        expect(@service.report_position).to eq 'ROBOT NOT PLACED'
      end

      it 'robot in position returns output string correctly' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :west
        expect(@service.report_position).to eq '2,1,WEST'
      end
    end

    context 'Robot Left' do
      it 'robot not placed doesnt do anything' do
        @service.robot_left
        robot = @service.robot
        expect(robot.x_coord).to be_nil
        expect(robot.y_coord).to be_nil
        expect(robot.facing).to be_nil
      end

      it 'robot placed, facing north, rotates to west' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :north
        @service.robot_left
        expect(@service.robot.facing).to eq :west
      end

      it 'robot placed, facing west, rotates to south' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :west
        @service.robot_left
        expect(@service.robot.facing).to eq :south
      end

      it 'robot placed, facing south, rotates to east' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :south
        @service.robot_left
        expect(@service.robot.facing).to eq :east
      end

      it 'robot placed, facing east, rotates to north' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :east
        @service.robot_left
        expect(@service.robot.facing).to eq :north
      end
    end

    context 'Robot Right' do
      it 'robot not placed doesnt do anything' do
        @service.robot_right
        robot = @service.robot
        expect(robot.x_coord).to be_nil
        expect(robot.y_coord).to be_nil
        expect(robot.facing).to be_nil
      end

      it 'robot placed, facing north, rotates to east' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :north
        @service.robot_right
        expect(@service.robot.facing).to eq :east
      end

      it 'robot placed, facing west, rotates to north' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :west
        @service.robot_right
        expect(@service.robot.facing).to eq :north
      end

      it 'robot placed, facing south, rotates to west' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :south
        @service.robot_right
        expect(@service.robot.facing).to eq :west
      end

      it 'robot placed, facing east, rotates to south' do
        @service.robot.x_coord = 2
        @service.robot.y_coord = 1
        @service.robot.facing = :east
        @service.robot_right
        expect(@service.robot.facing).to eq :south
      end
    end

    context 'Place Robot' do
      it 'place robot full text direction sets robot coordinates and facing correctly' do
        @service.place_robot([2,3,'EAST'])
        expect(@service.robot.x_coord).to eq 2
        expect(@service.robot.y_coord).to eq 3
        expect(@service.robot.facing).to eq :east
      end

      it 'place robot partial text direction sets robot coordinates and facing correctly' do
        @service.place_robot([4,2,'W'])
        expect(@service.robot.facing).to eq :west
        expect(@service.robot.x_coord).to eq 4
        expect(@service.robot.y_coord).to eq 2
      end

      it 'place robot full text direction lower case sets robot coordinates and facing correctly' do
        @service.place_robot([1,2,'south'])
        expect(@service.robot.facing).to eq :south
        expect(@service.robot.x_coord).to eq 1
        expect(@service.robot.y_coord).to eq 2
      end

      it 'place robot partial text direction sets robot coordinates and facing correctly' do
        @service.place_robot([4,2,'n'])
        expect(@service.robot.facing).to eq :north
        expect(@service.robot.x_coord).to eq 4
        expect(@service.robot.y_coord).to eq 2
      end

      it 'already placed - place robot changes robot location to new coordinates and facing' do
        @service.robot.x_coord = 1
        @service.robot.y_coord = 3
        @service.robot.facing = :south
        @service.place_robot([0,2,'west'])
        expect(@service.robot.facing).to eq :west
        expect(@service.robot.x_coord).to eq 0
        expect(@service.robot.y_coord).to eq 2
      end
    end

    context 'Move Robot' do
      it 'robot not placed doesnt do anything' do
        @service.move_robot
        robot = @service.robot
        expect(robot.x_coord).to be_nil
        expect(robot.y_coord).to be_nil
        expect(robot.facing).to be_nil
      end

      it 'robot facing east increments x_coord' do
        @service.robot.x_coord = 3
        @service.robot.y_coord = 2
        @service.robot.facing = :east
        @service.move_robot
        expect(@service.robot.x_coord).to eq 4
        expect(@service.robot.y_coord).to eq 2
        expect(@service.robot.facing).to eq :east
      end

      it 'robot facing west decrements x_coord' do
        @service.robot.x_coord = 3
        @service.robot.y_coord = 2
        @service.robot.facing = :west
        @service.move_robot
        expect(@service.robot.x_coord).to eq 2
        expect(@service.robot.y_coord).to eq 2
        expect(@service.robot.facing).to eq :west
      end

      it 'robot facing shout decrements y_coord' do
        @service.robot.x_coord = 3
        @service.robot.y_coord = 2
        @service.robot.facing = :south
        @service.move_robot
        expect(@service.robot.x_coord).to eq 3
        expect(@service.robot.y_coord).to eq 1
        expect(@service.robot.facing).to eq :south
      end

      it 'robot facing north increments y_coord' do
        @service.robot.x_coord = 3
        @service.robot.y_coord = 2
        @service.robot.facing = :north
        @service.move_robot
        expect(@service.robot.x_coord).to eq 3
        expect(@service.robot.y_coord).to eq 3
        expect(@service.robot.facing).to eq :north
      end
    end
  end
end
